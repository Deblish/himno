<<<<<<< HEAD
Aaron Padilla

=======
>>>>>>> rama1
I
Ciña ¡Oh Patria! tus sienes de oliva
De la paz el arcángel divino,
Que en el cielo tu eterno destino
Por el dedo de Dios se escribió.
Mas si osare un extraño enemigo
Profanar con su planta tu suelo,
Piensa ¡Oh Patria querida! que el cielo
Un soldado en cada hijo te dio.

V
¡Guerra, guerra sin tregua al que intente
De la patria manchar los blasones!,
¡Guerra, guerra! los patrios pendones
En las olas de sangre empapad.
¡Guerra, guerra! en el monte, en el valle,
Los cañones horrísonos truenen
Y los ecos sonoros resuenen
Con las voces de ¡Unión! ¡Libertad!
=======
II
En sangrientos combates los viste
Por tu amor palpitando sus senos,
Arrostrar la metralla serenos
Y la muerte o la gloria buscar.
Si el recuerdo de antiguas hazañas
De tus hijos inflama la mente,
Los laureles del triunfo tu frente
Volverán inmortales a ornar.

III
Como al golpe del rayo la encina
Se derrumba hasta el hondo torrente,
La discordia vencida, impotente,
A los pies del arcángel cayó.
Ya no más de tus hijos la sangre
Se derrame en contienda de hermanos;
Solo encuentre el acero en tus manos
Quien tu nombre sagrado insultó.

IV
Del guerrero inmortal de Zempoala
Te defiende la espada terrible,
Y sostiene su brazo invencible
Tu sagrado pendón tricolor.
Él será del feliz mexicano
En la paz y en la guerra el caudillo,
Porque él supo sus armas de brillo
Circundar en los campos de honor.
>>>>>>> rama2

Letra completa del Himno Nacional Mexicano
Coro

Mexicanos, al grito de guerra
El acero aprestad y el bridón;
Y retiemble en sus centros la tierra
Al sonoro rugir del cañón.
